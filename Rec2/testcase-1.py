from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from PIL import ImageFile
import matplotlib.pyplot as plt
import datetime

img_width, img_height = 28, 28
train_data_dir = './TC1/person/train'
validation_data_dir = './TC1/person/Validation'
train_samples = 1000
validation_samples = 100
epoch = 2

model = Sequential()
model.add(Conv2D(8, (3, 3), input_shape = (28, 28, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Conv2D(16, (3, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Conv2D(64, (3, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Flatten())
model.add(Dense(units = 224, activation = 'relu'))
model.add(Dense(units = 4, activation = 'softmax'))
model.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
test_datagen = ImageDataGenerator(rescale=1./255)
train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')

ImageFile.LOAD_TRUNCATED_IMAGES = True
a = datetime.datetime.now().replace(microsecond=0)
history = model.fit_generator(train_generator,
steps_per_epoch = 1000,
epochs = 2,
validation_data = validation_generator,
validation_steps = 100)
model.save_weights('./models/model_1000_2.h5')
b = datetime.datetime.now().replace(microsecond=0)
print(b-a)
plt.plot(history.history['acc'])

#Accuracy and Loss
plt.plot(history.history['val_acc'])
plt.title('model_accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train','test'],loc='upper left')
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
