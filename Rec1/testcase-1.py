from keras.models import Sequential
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.models import load_model
from keras.preprocessing.image import ImageDataGenerator
from PIL import ImageFile
import matplotlib.pyplot as plt
import datetime
import pywavefront
from pywavefront import visualization

var f1
img_width, img_height = 28, 28
train_data_dir = './TC1/person/train'
validation_data_dir = './TC1/person/Validation'
train_samples = 1000
validation_samples = 100
epoch = 2

model = Sequential()
model.add(Conv2D(8, (3, 3), input_shape = (28, 28, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Conv2D(16, (3, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Conv2D(64, (3, 3), activation = 'relu'))
model.add(MaxPooling2D(pool_size = (2, 2)))
model.add(Flatten())
model.add(Dense(units = 224, activation = 'relu'))
model.add(Dense(units = 4, activation = 'softmax'))
model.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
test_datagen = ImageDataGenerator(rescale=1./255)
train_generator = train_datagen.flow_from_directory(
        train_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')

ImageFile.LOAD_TRUNCATED_IMAGES = True
a = datetime.datetime.now().replace(microsecond=0)
history = model.fit_generator(train_generator,
steps_per_epoch = 1000,
epochs = 2,
validation_data = validation_generator,
validation_steps = 100)
model.save_weights('./models/model_1000_1.h5')
b = datetime.datetime.now().replace(microsecond=0)
print(b-a)
plt.plot(history.history['acc'])

#Accuracy and Loss
plt.plot(history.history['val_acc'])
plt.title('model_accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train','test'],loc='upper left')
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()

#freethrow
train_data_dir1 = './TC1/person/train/freethrow'
validation_data_dir1 = './TC1/person/Validation/freethrow'
img_width, img_height = 28, 28
train_samples = 100
validation_samples = 10
epoch = 2

model = Sequential()
model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(2, 2))
model.add(Convolution2D(32, 5, 5, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(4, activation='softmax'))
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir1,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')

validation_generator = test_datagen.flow_from_directory(
        validation_data_dir1,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
model.fit_generator(
        train_generator,
        samples_per_epoch=train_samples,
        nb_epoch=epoch,
        validation_data=validation_generator,
        nb_val_samples=validation_samples)

model.save_weights('./models/personft.h5')

#layup
train_data_dir2 = './TC1/person/train/layup'
validation_data_dir2 = './TC1/person/Validation/layup'
img_width, img_height = 28, 28
train_samples = 100
validation_samples = 10
epoch = 2

model = Sequential()
model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(2, 2))
model.add(Convolution2D(32, 5, 5, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(4, activation='softmax'))
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir2,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir2,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
model.fit_generator(
        train_generator,
        samples_per_epoch=train_samples,
        nb_epoch=epoch,
        validation_data=validation_generator,
        nb_val_samples=validation_samples)

model.save_weights('./models/personlp.h5')

#dribble
train_data_dir3 = './TC1/person/train/dribble'
validation_data_dir3 = './TC1/person/Validation/dribble'
img_width, img_height = 28, 28
train_samples = 100
validation_samples = 10
epoch = 2

model = Sequential()
model.add(Convolution2D(16, 5, 5, activation='relu', input_shape=(img_width, img_height, 3)))
model.add(MaxPooling2D(2, 2))
model.add(Convolution2D(32, 5, 5, activation='relu'))
model.add(MaxPooling2D(2, 2))
model.add(Flatten())
model.add(Dense(128, activation='relu'))
model.add(Dense(4, activation='softmax'))
model.compile(loss='binary_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

train_datagen = ImageDataGenerator(
        rescale=1./255,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
test_datagen = ImageDataGenerator(rescale=1./255)

train_generator = train_datagen.flow_from_directory(
        train_data_dir1,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')
validation_generator = test_datagen.flow_from_directory(
        validation_data_dir1,
        target_size=(img_width, img_height),
        batch_size=32,
        class_mode='categorical')

model.fit_generator(
        train_generator,
        samples_per_epoch=train_samples,
        nb_epoch=epoch,
        validation_data=validation_generator,
        nb_val_samples=validation_samples)

model.save_weights('./models/persondr.h5')

#prediction
test_data_dir = './data/test.jpg'
img = cv2.imread(sys.argv[1])
img = cv2.resize(img, (img_width, img_height))

model.load_weights('./models/model_1000_1.h5')
	arr = numpy.array(img).reshape((img_width,img_width,3))
    arr = numpy.expand_dims(arr,axis=0)
	prediction = model.prediction(arr)[0]
	bestclass = ''
	bestconf = -1
	prbclass=''

	for n in [0,1,2,3]:
    	if (prediction[n] > bestconf):
    		bestclass = str(n)
    		bestconf = prediction[n]

	if bestclass=='0':
		prbclass = 'freethrow'
        print 'Posture : ' + prbclass
        model.load_weights('./models/personft.h5')
	    arr = numpy.array(img).reshape((img_width,img_width,3))
        arr = numpy.expand_dims(arr,axis=0)
	    prediction = model.prediction(arr)[0]
	    bestclass = ''
	    bestconf = -1
	    prbclass=''
	    for n in [0,1,2,3]:
    	    if (prediction[n] > bestconf):
    		    bestclass = str(n)
    		    bestconf = prediction[n]
	    if bestclass=='0':
		    prbclass = 'left' 
            f1 = pywavefront.Wavefront('./3d Models/ftleft.obj')
	        visualization.draw(f1)
	    elif bestclass == '1':
		    prbclass = 'right'
            f1 = pywavefront.Wavefront('./3d Models/ftright.obj')
	        visualization.draw(f1)
	    elif bestclass == '2':
		    prbclass = 'front'
            f1 = pywavefront.Wavefront('./3d Models/ftfront.obj')
	        visualization.draw(f1)
	     elif bestclass == '3':
		    prbclass = 'back'
            f1 = pywavefront.Wavefront('./3d Models/ftback.obj')
	        visualization.draw(f1)
        else :
	 	    prbclass = 'null'
        print 'Accuracy: '+ str(bestconf*100) 
	elif bestclass == '1':
	    prbclass = 'layup'
        print 'Posture : ' + prbclass
        model.load_weights('./models/personlp.h5')
	    arr = numpy.array(img).reshape((img_width,img_width,3))
        arr = numpy.expand_dims(arr,axis=0)
	    prediction = model.prediction(arr)[0]
	    bestclass = ''
	    bestconf = -1
	    prbclass=''
	    for n in [0,1,2,3]:
    	    if (prediction[n] > bestconf):
    		    bestclass = str(n)
    		    bestconf = prediction[n]
	    if bestclass=='0':
		    prbclass = 'left' 
            f1 = pywavefront.Wavefront('./3d Models/lpleft.obj')
	        visualization.draw(f1)
	    elif bestclass == '1':
		    prbclass = 'right'
            f1 = pywavefront.Wavefront('./3d Models/lpright.obj')
	        visualization.draw(f1)
	    elif bestclass == '2':
		    prbclass = 'front'
            f1 = pywavefront.Wavefront('./3d Models/lpfront.obj')
	        visualization.draw(f1)
	     elif bestclass == '3':
		    prbclass = 'back'
            f1 = pywavefront.Wavefront('./3d Models/lpback.obj')
	        visualization.draw(f1)
        else :
	 	    prbclass = 'null'
        print 'Accuray:  '+ str(bestconf*100) 
	elif bestclass == '2':
		prbclass = 'dribble'
        print 'Posture : ' + prbclass
        model.load_weights('./models/persondr.h5')
	    arr = numpy.array(img).reshape((img_width,img_width,3))
        arr = numpy.expand_dims(arr,axis=0)
	    prediction = model.prediction(arr)[0]
	    bestclass = ''
	    bestconf = -1
	    prbclass=''
	    for n in [0,1,2,3]:
        if (prediction[n] > bestconf):
    		    bestclass = str(n)
    		    bestconf = prediction[n]
	    if bestclass=='0':
		    prbclass = 'left' 
            f1 = pywavefront.Wavefront('./3d Models/drleft.obj')
	        visualization.draw(f1)
	    elif bestclass == '1':
		    prbclass = 'right'
            f1 = pywavefront.Wavefront('./3d Models/drright.obj')
	        visualization.draw(f1)
	    elif bestclass == '2':
		    prbclass = 'front'
            f1 = pywavefront.Wavefront('./3d Models/drfront.obj')
	        visualization.draw(f1)
	     elif bestclass == '3':
		    prbclass = 'back'
            f1 = pywavefront.Wavefront('./3d Models/drback.obj')
	        visualization.draw(f1)
        else :
	 	    prbclass = 'null'
        print 'Accuray:  '+ str(bestconf*100)
	else :
	 	prbclass = 'null'