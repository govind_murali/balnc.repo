from keras.models import Sequential
from keras.models import Model
from keras.layers import Conv2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.preprocessing.image import ImageDataGenerator
from keras.models import load_model
from PIL import ImageFile
import matplotlib.pyplot as plt
import datetime

classes = ['DR','FT','LP']

classifier = Sequential()
classifier.add(Conv2D(8, (3, 3), input_shape = (224, 224, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
classifier.add(Conv2D(16, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
classifier.add(Conv2D(64, (3, 3), activation = 'relu'))
classifier.add(MaxPooling2D(pool_size = (2, 2)))
classifier.add(Flatten())
classifier.add(Dense(units = 224, activation = 'relu'))
classifier.add(Dense(units = 4, activation = 'softmax'))
classifier.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])

train_datagen = ImageDataGenerator(rescale = 1./255,horizontal_flip = True)
valid_datagen = ImageDataGenerator(rescale = 1./255,horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('./TC2/arun/train',
target_size = (224, 224),
batch_size = 5,
shuffle = True,
color_mode="rgb",
class_mode = 'categorical')

validation_set = valid_datagen.flow_from_directory('/TC2/arun/Validation',
target_size = (224, 224),
batch_size = 5,
shuffle = True,
color_mode="rgb",
class_mode = 'categorical')

ImageFile.LOAD_TRUNCATED_IMAGES = True
a = datetime.datetime.now().replace(microsecond=0)

history = classifier.fit_generator(training_set,
steps_per_epoch = 1000,
epochs = 2,
validation_data = validation_set,
validation_steps = 100)
classifier.save_weights('./models/model_1000_32.h5')
b = datetime.datetime.now().replace(microsecond=0)
print(b-a)
plt.plot(history.history['acc'])
plt.plot(history.history['val_acc'])
plt.title('model_accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train','test'],loc='upper left')
plt.show()
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'validation'], loc='upper left')
plt.show()
#second classifier
classifier2 = Sequential()
classifier2.add(Conv2D(8, (3, 3), input_shape = (224, 224, 3), activation = 'relu'))
classifier2.add(MaxPooling2D(pool_size = (2, 2)))
classifier2.add(Conv2D(16, (3, 3), activation = 'relu'))
classifier2.add(MaxPooling2D(pool_size = (2, 2)))
classifier2.add(Conv2D(64, (3, 3), activation = 'relu'))
classifier2.add(MaxPooling2D(pool_size = (2, 2)))
classifier2.add(Flatten())
classifier2.add(Dense(units = 224, activation = 'relu'))
classifier2.add(Dense(units = 4, activation = 'softmax'))
classifier2.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])
classifier2.load_weights('./model_1000_32.h5')

train_datagen = ImageDataGenerator(rescale = 1./255,horizontal_flip = True)
valid_datagen = ImageDataGenerator(rescale = 1./255,horizontal_flip = True)
test_datagen = ImageDataGenerator(rescale = 1./255)

training_set = train_datagen.flow_from_directory('./TC2/ashik/train',
target_size = (224, 224),
batch_size = 5,
shuffle = True,
color_mode="rgb",
class_mode = 'categorical')

validation_set = valid_datagen.flow_from_directory('/TC2/ashik/Validation',
target_size = (224, 224),
batch_size = 5,
shuffle = True,
color_mode="rgb",
class_mode = 'categorical')

history2 = classifier2.fit_generator(training_set,
steps_per_epoch = 1000,
epochs = 2,
validation_data = validation_set,
validation_steps = 100)
classifier2.save_weights('./models/model_1000_32.h5')
composed_model= Model(
inputs=[classifier.input,classifier2.input],
output=[classifier2.output]
)
composed_model.compile(optimizer = 'rmsprop', loss = 'categorical_crossentropy', metrics = ['accuracy'])
composed_model.save('./models/comp_model_1000_32.h5')





